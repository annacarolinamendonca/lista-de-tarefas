//
//  TarefaUserDefaults.swift
//  Lista de Tarefas
//
//  Created by Anna Carolina on 21/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class TarefaUserDefaults {
    
    let chave = "listaTarefas"
    var tarefas: [String] = []
    
    func remover(indice: Int) {
        
        //recuperar tarefa ja salva antes de salvar uma nova tarefa
        tarefas = listar()
        
        tarefas.remove(at: indice)
        UserDefaults.standard.set( tarefas, forKey: chave)
        
    }
    
    func Salvar (tarefa: String) {
        
        //recuperar tarefa ja salva antes de salvar uma nova tarefa
        tarefas = listar()
        
        //Salvar tarefa
        tarefas.append( tarefa )
        UserDefaults.standard.set( tarefas, forKey: chave)
        
    }
    
    func listar() -> Array <String>{
        
        if let dados = UserDefaults.standard.value(forKey: chave) as? Array<String> {
            return dados
        }
        
        return []
    }
}

