//
//  TarefasTableViewController.swift
//  Lista de Tarefas
//
//  Created by Anna Carolina on 21/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class TarefasTableViewController: UITableViewController {

    var tarefas: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete{
           let tarefa = TarefaUserDefaults()
            tarefa.remover(indice: indexPath.row)
            atualizar()
        }
        
    }
    
    //atualizar lista de tarefasas depois de remover um item
    
    func atualizar() {
        let tarefa = TarefaUserDefaults()
        tarefas = tarefa.listar()
        tableView.reloadData()
        
    }
    
    //listar tarefa quando a tela aparecer para o usuario
    
    override func viewDidAppear(_ animated: Bool) {
        atualizar()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tarefas.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celula = tableView.dequeueReusableCell(withIdentifier: "celula", for: indexPath)

        // Configure the cell...
        
        celula.textLabel?.text = tarefas[indexPath.row]

        return celula
    }

}
