//
//  CadastroTarefaViewController.swift
//  Lista de Tarefas
//
//  Created by Anna Carolina on 21/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class CadastroTarefaViewController: UIViewController {
    
    
    @IBOutlet weak var tarefaCampo: UITextField!
    
    @IBAction func adicionarTarefa(_ sender: Any) {
        
        if let textoDigitado = tarefaCampo.text{
            
            let tarefa = TarefaUserDefaults ()
            tarefa.Salvar(tarefa: textoDigitado)
            tarefaCampo.text = ""
        
            let dados = tarefa.listar()
            
            print(dados)
            
            
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
